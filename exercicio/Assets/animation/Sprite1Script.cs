using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprite1Script : MonoBehaviour
{
    public float jumpForce;
    private Rigidbody2D body;
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        body.isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            body.isKinematic = false;
            body.AddForce(new Vector2(0, jumpForce));
        }

        this.transform.rotation = Quaternion.Euler(0, 0, body.velocity.y * 3);
    }

}
